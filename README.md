# avareport_statistics

This project contains multiple subfolders for the statistical analysis of the avalanche report data-set.

- EAWS Matrix
- Euregio neighbours
- Frequency of avalanche problems with exposition
- Frequency of danger ratings
- Satzkatalog
- Animated video of danger rating map


For a more detailed description of each analysis you can find a readme in the related subfolder.

