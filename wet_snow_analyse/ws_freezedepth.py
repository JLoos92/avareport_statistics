

import numpy as np
import pandas as pd
from ws_mswater import df_lwc
from ws_snowheight import df_hs


df_lwc.fillna(value=pd.np.nan,inplace=True)

freeze_depth_bool = df_lwc[df_lwc==0]
freeze_depth_bool = freeze_depth_bool.notna()
freeze_depth = df_hs[freeze_depth_bool]
freeze_depth = freeze_depth.min().to_frame().T
    
