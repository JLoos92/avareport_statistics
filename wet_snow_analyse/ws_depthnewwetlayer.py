#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 12:39:52 2020

@author: Schmulius
"""

import numpy as np
import pandas as pd
from ws_mswater import df_lwc
from ws_snowheight import df_hs,df_hs_new


# LWC > 2 1 schicht
df_lwc.fillna(value=pd.np.nan,inplace=True)


depth_new_wet_bool = df_lwc[df_lwc!=0]
depth_new_wet_bool = depth_new_wet_bool.notna()
depth_new_wet = df_hs_new[depth_new_wet_bool]
depth_new_wet = depth_new_wet.idxmax()