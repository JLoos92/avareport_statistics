#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  7 09:52:59 2020

@author: Schmulius
"""

import pandas as pd
from ws_parameter import df_date_2
from main_instability import result_cond1,result_cond2,result_cond3,result_cond4


def filter_ampm(instability_index_depth):
    
    '''
    Filter which uses input from main_instability modules and filter's
    after date. Every day is splitted in half e.g. AM:00:00:00 - 12:00:00
    PM:12:00:00 - 00:00:00.
    
    Resampling of 12 indices for both AM and PM. Filter max value which is
    relevant for instability indices.
        
    '''
    
    # make am pm mask; count occurence every 12 hours
    instability_index_depth = instability_index_depth.T
    df = df_date_2.reset_index()
    df = pd.concat([instability_index_depth,df],axis=1)
    df = df.set_index('Date')
    df.columns = ['Value']
   
    df_counts = df.resample('12H')['Value'].count().to_frame()    
    df_counts.index = pd.to_datetime(df_counts.index)
    df_counts['Daytime'] = df_counts.index.hour
    df_counts = df_counts.copy()
    df_counts.Daytime[df_counts.Daytime == 0] = 'AM'
    df_counts.Daytime[df_counts.Daytime == 12] = 'PM'
    df_counts.index = df_counts.index.date
    
    return df_counts




counts_cond1 = filter_ampm(result_cond1)
counts_cond2 = filter_ampm(result_cond2)
counts_cond3 = filter_ampm(result_cond3)
counts_cond4 = filter_ampm(result_cond4)