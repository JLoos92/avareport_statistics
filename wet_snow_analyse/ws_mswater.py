import numpy as np
import pandas as pd
from ws_parameter import date_new,lwc_V,df_lwc_V
from ws_snowheight import df_hs,df_hs_new


# calculate thickness of layers
# create dataframe
df_lwc = pd.DataFrame(lwc_V).T 
df_lwc = df_lwc.drop(df_lwc.index[0])
df_lwc = df_lwc.drop(df_lwc.index[0])
df_lwc = df_lwc.apply(pd.to_numeric,errors='coerce')
#df_lwc = df_lwc.convert_objects(convert_numeric=True)

df_lwc = df_lwc_V.apply(pd.to_numeric,errors='coerce')
#df_lwc = df_lwc_V.convert_objects(convert_numeric=True)

snow_thick = df_hs_new.diff(axis=0,periods=1)
#snow_thick = snow_thick.drop(snow_thick.index[0])
snow_thick.update(df_hs_new[:1])
ms_water = snow_thick*df_lwc
ms_water = ms_water.sum()*0.1
ms_water = ms_water.to_frame().T

