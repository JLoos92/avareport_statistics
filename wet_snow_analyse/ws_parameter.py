import numpy as np
import pandas as pd

 
    
listlines = list()

with open('input/GESS2.pro','r') as f:
    for line in f.readlines()[47:]:
        listlines.append(line.strip())

 
# arrange data array
listline_idx = [k for k, e in enumerate(listlines) if '0501,1,0' == e] 
listline_idx_dates = [k-1 for k in listline_idx]

[k for k, i in enumerate(listlines) if i not in listline_idx_dates]

for i in reversed(listline_idx_dates):
    del listlines[i]

splitted_listline = [s.split(',') for s in listlines]

date_new     = [i for i in splitted_listline if '0500' == i[0]]
hs_new       = [i for i in splitted_listline if '0501' == i[0]]
hs_new       = [i for i in hs_new if '0' != i[2]]
temperature  = [i for i in splitted_listline if '0503' == i[0]]
elem_id      = [i for i in splitted_listline if '0504' == i[0]]
lwc_V        = [i for i in splitted_listline if '0506' == i[0]]
sk_38       = [i for i in splitted_listline if '0533' == i[0]]


# Arange Dates
df_date = pd.DataFrame(date_new).T 
df_date = df_date.drop(df_date.index[0])
#df_date = df_date.convert_objects(convert_numeric=True)
df_date = df_date.T
df_date.columns = ['Date']

df_date['Date'] = pd.to_datetime(df_date['Date'],format='%d.%m.%Y %H:%M:%S')
df_date_2 = df_date.set_index('Date').resample('H').asfreq()


df_hs_new = pd.DataFrame(hs_new).set_index(df_date['Date']).resample('H').asfreq()
df_hs_new.drop(df_hs_new[[0,1]],axis=1,inplace=True)
df_hs_new = df_hs_new.reset_index(drop=True).T

df_temperature = pd.DataFrame(temperature).set_index(df_date['Date']).resample('H').asfreq()
df_temperature.drop(df_temperature[[0,1]],axis=1,inplace=True)
df_temperature = df_temperature.reset_index(drop=True).T

df_elem_id = pd.DataFrame(elem_id).set_index(df_date['Date']).resample('H').asfreq()
df_elem_id.drop(df_elem_id[[0,1]],axis=1,inplace=True)
df_elem_id = df_elem_id.reset_index(drop=True).T

df_sk38_id = pd.DataFrame(sk_38).set_index(df_date['Date']).resample('H').asfreq()
df_sk38_id.drop(df_sk38_id[[0,1]],axis=1,inplace=True)
df_sk38_id = df_sk38_id.reset_index(drop=True).T

df_lwc_V = pd.DataFrame(lwc_V).set_index(df_date['Date']).resample('H').asfreq()
df_lwc_V.drop(df_lwc_V[[0,1]],axis=1,inplace=True)
df_lwc_V = df_lwc_V.reset_index(drop=True).T






    
