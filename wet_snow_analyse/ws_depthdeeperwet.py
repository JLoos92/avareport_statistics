import numpy as np
import pandas as pd
from ws_mswater import df_lwc
from ws_snowheight import df_hs


# LWC > 2 1 schicht
df_lwc.fillna(value=pd.np.nan,inplace=True)


depth_deeper_wet_bool = df_lwc[df_lwc!=0]
depth_deeper_wet_bool = depth_deeper_wet_bool.notna()
depth_deeper_wet = df_hs[depth_deeper_wet_bool]
depth_deeper_wet = depth_deeper_wet.min().to_frame().T



