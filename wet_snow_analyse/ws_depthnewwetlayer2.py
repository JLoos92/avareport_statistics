#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 12:39:52 2020

@author: Schmulius
"""

import numpy as np
import pandas as pd
from ws_mswater import df_lwc
from ws_snowheight import df_hs,df_hs_new


# LWC > 2 1 schicht
df_lwc.fillna(value=pd.np.nan,inplace=True)


depth_new_wet2_bool = df_lwc[df_lwc>2]
depth_new_wet2_bool = depth_new_wet2_bool.notna()
depth_new_wet2 = df_hs_new[depth_new_wet2_bool]
depth_new_wet2 = depth_new_wet2.idxmax()