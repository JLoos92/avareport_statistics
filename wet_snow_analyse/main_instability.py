import numpy as np
import numpy as np
import pandas as pd
import sys
import csv
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.dates import DateFormatter
from matplotlib import rc

#import seaborn as sns
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.ticker as ticker
import matplotlib.dates as dates


#modules
from ws_mswater import df_lwc
from ws_lwc import lwc_index
from ws_isothermic import df_temp_bool
from ws_depthwet import depth_wet
from ws_depthnewwetlayer2 import depth_new_wet2
from ws_snowheight import max_hs,df_date,df_hs
from ws_depthnewwetlayer import depth_new_wet




'''
Describing instability types from modules:
    
    
    Type 1:
    --> LWC index higher than 1 and not isothermic (large loose avalanches)
        
    Type 2:
    --> LWC index higher than 1 and isothermic (large full depth wet avalanches)
        
    Type 3:
    --> LWC index between 0.3 and 1
        
    Type 4:
    --> LWC bigger than 2% , Type 1 2 3 are not sufficient and the snowpack is not
    totally wet
        
    Type 1 thick:
        
    Type 2 thick:
        
    Type 3 thick:
        
    Type 4 thick:
        
    Total wet instability thick:
        
'''
  
depth_wet = depth_wet.to_frame().T 

#isotherm for less than 5 days
df_temp_bool2 = df_temp_bool.T
df_temp_bool2.columns = ['days']
df_temp_bool2['days'] = df_temp_bool2['days'].astype(int)
df_temp_bool2['consecutive'] = df_temp_bool2.days.groupby((df_temp_bool2.days 
            != df_temp_bool2.days.shift()).cumsum()).transform('size')*df_temp_bool2.days
df_temp_days = ((df_temp_bool2['consecutive']<=120) & (df_temp_bool2['consecutive'] != 0)).to_frame()

#not isotherm for at least 24h
df_temp_bool1 = df_temp_bool.T
df_temp_bool1 = ~df_temp_bool1
df_temp_bool1.columns = ['days']
df_temp_bool1['days'] = df_temp_bool1['days'].astype(int)
df_temp_bool1['consecutive'] = df_temp_bool1.days.groupby((df_temp_bool1.days 
            != df_temp_bool1.days.shift()).cumsum()).transform('size')*df_temp_bool1.days
df_temp_days1 = ((df_temp_bool1['consecutive']>=24) & (df_temp_bool1['consecutive'] != 0)).to_frame()


# transform
df_temp_days  = df_temp_days.T
df_temp_days1 = df_temp_days1.T




# Case 1
lwc_cond1 = (lwc_index>=1)
all_cond1 = lwc_cond1.append(df_temp_days1).T
all_cond1.columns =['lwc_i','isot']
result_cond1 = all_cond1.index[(all_cond1['lwc_i'] == True) & (all_cond1['isot'] == True)]
result_cond1 = depth_wet[result_cond1]

# Case 2
lwc_cond2 = (lwc_index>=1)
all_cond2 = lwc_cond2.append(df_temp_days).T
all_cond2.columns =['lwc_i','isot']
result_cond2 = all_cond2.index[(all_cond2['lwc_i'] == True) & (all_cond2['isot'] == True)]
result_cond2 = depth_wet[result_cond2]

# Case 3
lwc_cond3 = (lwc_index<=1) & (lwc_index>=0.3)
lwc_cond3 = lwc_cond3.T
lwc_cond3.columns = ['bool']
result_cond3 = lwc_cond3.index[lwc_cond3['bool'] == True]
result_cond3 = depth_new_wet[result_cond3]

# Case 4
lwc_cond4 = depth_wet>=max_hs
lwc_cond4_2 = df_lwc>2
lwc_cond4_2 = lwc_cond4_2.any().to_frame().T
all_cond4 = lwc_cond4.append(lwc_cond4_2)
all_cond4 = all_cond4.all()
result_cond4 = all_cond4.index[all_cond4 == True]

#drop index if in result4 (from cond1,cond2,cond3)
a = [x for x in result_cond4 if x in result_cond1]
b = [x for x in result_cond4 if x in result_cond2]
c = [x for x in result_cond4 if x in result_cond3]

result_cond4 = result_cond4.drop(a)
result_cond4 = result_cond4.drop(b)
result_cond4 = result_cond4.drop(c)
result_cond4 = depth_wet[result_cond4]


# index for thickness
#thickness_cond4 = max_hs[result_cond4]


np_nan = np.full(5933, np.nan)
np_nan = pd.DataFrame(np_nan).T

result_cond1 = np_nan.fillna(result_cond1)
result_cond2 = np_nan.fillna(result_cond2)
result_cond3 = np_nan.fillna(result_cond3)
result_cond4 = np_nan.fillna(result_cond4)


# Plots
plt.plot(max_hs.iloc[0,:],'b-')
#plt.plot(depth_wet.iloc[0,:],'r*')
plt.plot(result_cond1.iloc[0,:],'k+')
plt.plot(result_cond2.iloc[0,:],'y+')
plt.plot(result_cond3.iloc[0,:],'g+')
plt.plot(result_cond4.iloc[0,:],'c+')
