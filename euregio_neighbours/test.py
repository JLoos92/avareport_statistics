#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  3 14:47:51 2020

@author: Schmulius
"""

df_neighbours_org.geometry[7]

list_itrp = []

for k in range(len(lines)):

    x,y = zip(*list(lines[k].coords))
    
    x = np.asarray(x)
    y = np.asarray(y)
    
    points = np.asarray(np.c_[x, y])
    
    clf = NearestNeighbors(4).fit(points)
    G = clf.kneighbors_graph()
    
    T = nx.from_scipy_sparse_matrix(G)
    
    paths = [list(nx.dfs_preorder_nodes(T, i)) for i in range(len(points))]
    
    mindist = np.inf
    minidx = 0
    
    for i in range(len(points)):
        p = paths[i]           # order of nodes
        ordered = points[p]    # ordered nodes
        # find cost of that order by the sum of euclidean distances between points (i) and (i+1)
        cost = (((ordered[:-1] - ordered[1:])**2).sum(1)).sum()
        if cost < mindist:
            mindist = cost
            minidx = i
    
    opt_order = paths[minidx]
    
    
    xx = x[opt_order]
    yy = y[opt_order]
       
    point_xy = [Point(xy) for xy in zip(xx,yy)]
    linestring_xy = LineString(point_xy)
    list_itrp.append(linestring_xy)
    

