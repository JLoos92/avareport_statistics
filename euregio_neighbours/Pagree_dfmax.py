#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 14:23:56 2020

@author: Schmulius
"""

import numpy as np
import pandas as pd



# Import data

# import data as dataframe
path   = 'avalanche_report_statistics.csv'
path_2 = 'neighboursALBINA.csv'
path_3 = 'shape_without0.csv'


df_stats_org        = pd.read_csv(path,header=0,sep=';')
df_neighbours   = pd.read_csv(path_2,header=0,sep=',')
df_area         = pd.read_csv(path_3,header=0,sep=';')

# arange and filter data
df_neighbours = df_neighbours.drop(df_neighbours.columns[[0]],axis=1)
df_stats = df_stats_org[['BulletinId','Date','Daytime','Subregion',
                     'Region','DangerRatingBelow',
                     'DangerRatingAbove']]

#df_stats = df_stats.drop_duplicates(subset=['BulletinId'])

df_stats = df_stats.replace(to_replace =['no_snow'],  
                            value ="0")
df_stats = df_stats.replace(to_replace =['low'],  
                            value ="1")
df_stats = df_stats.replace(to_replace =['moderate'],  
                            value ="2")
df_stats = df_stats.replace(to_replace =['considerable'],  
                            value ="3")
df_stats = df_stats.replace(to_replace =['high'],  
                            value ="4")
df_stats = df_stats.replace(to_replace =['very_high'],  
                            value ="5")

# get max of daytime danger ratings
df_max = df_stats[['DangerRatingBelow','DangerRatingAbove']].max(axis=1).to_frame()
df_max.columns = ['max_danger']
df_max = df_max['max_danger']

#concat df's
df_stats = pd.concat([df_stats,df_max],axis=1)

df_stats['Subregion'] = pd.to_numeric(df_stats['Subregion'])
df_stats['Subregion'] = df_stats['Subregion'].astype(str)
df_stats['Subregion'] = df_stats.Subregion.str.zfill(2)
df_stats['NewRegion'] = df_stats['Region'] + '-' + df_stats['Subregion']

# filter dates and regions
df_regions = df_stats[['Date','NewRegion','max_danger']]
df_pagree = []

# loop for pairs of each date

for i,j in zip(df_neighbours['V1'].iloc[:],df_neighbours['V2'].iloc[:],):

    df_i = df_regions.groupby(['NewRegion']).get_group(str(i)).reset_index(drop=True)
    df_j = df_regions.groupby(['NewRegion']).get_group(str(j)).reset_index(drop=True)
    df_diff = df_i['max_danger'].astype(int) - df_j['max_danger'].astype(int)
    df_diff = df_diff.value_counts()
    df_sum  = df_diff.sum()
    pAgree  = df_diff.iloc[0] / df_sum
    df_pagree.append(pAgree)
    
#Arange new pagree  
df_pagree = pd.DataFrame(df_pagree)
df_pagree.columns = ['pAgree']

#New dataframe with regions and pAgree
df_all = pd.concat([df_neighbours,df_pagree],axis=1)




    

    










