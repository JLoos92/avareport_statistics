#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 16:39:40 2020

@author: Schmulius
"""

import pandas as pd
import numpy as np
import geopandas as gpd
import matplotlib.pyplot as plt
from geopandas import GeoDataFrame
from shapely.geometry import Point,LineString
from Pagree_dfmax import df_all
from math import log
from sklearn.neighbors import NearestNeighbors
import networkx as nx
from collections import OrderedDict

#Test
# input
df_raw = pd.read_csv('input/Outlines.csv',sep=',')
#df_raw['X1'] = np.trunc(1000*df_raw.X1)/1000
#pd.options.display.float_format = '{%.4f}'.format
##float( '%.4f'%(df_raw.X1.iloc[0]) )
df_raw['X1'] = df_raw['X1'].map(lambda x: '%.4f' % x).astype(float)
df_raw['X2'] = df_raw['X2'].map(lambda y: '%.4f' % y).astype(float)

df_raw = df_raw.drop_duplicates(subset=['X1','X2','RegionCode'])

# get points
df_raw = df_raw.drop(df_raw.columns[[0]],axis=1)
df_comb = df_raw.drop(['RegionCode'],axis=1)
df_comv = df_comb.values.tolist()

# transform points to Geodataframe
geometry = [Point(xy) for xy in zip(df_raw.X1,df_raw.X2)]
df_geoframe = GeoDataFrame(df_raw.RegionCode,geometry=geometry)

# groupby region
df_grouped = df_geoframe.groupby(['RegionCode'])['geometry'].apply(lambda x: LineString(x.tolist()))
df = GeoDataFrame(df_grouped,geometry='geometry')

# group after points
df_areas_grouped = df_raw.groupby(['X1','X2']).agg(lambda column: "__".join(column)).reset_index()

#rearange
df_aggs = df_areas_grouped.groupby(['RegionCode'])[['X1','X2']].apply(lambda x: x.values.tolist())
df_aggs = df_aggs.reset_index()


# sort after original output.csv
df_aggs.columns = ['RegionCode','Vals'] 

df_all_2 = []
for m in range(df_aggs.Vals.size):
    
    df_bool_1 = [i for i in df_comv if i in df_aggs['Vals'][m]]
    df_all_2.append(df_bool_1)    


# set up for geodataframe        
df_neighbours_test           = pd.DataFrame({0:df_all_2}) 
df_aggs_2 = pd.concat([df_aggs['RegionCode'],df_neighbours_test],axis=1)

# merge areas
df_merge = df_all['V1']+'__'+df_all['V2']
df_merge = df_merge.to_frame()
df_merge.columns = ['merged']

# index and fit
list_merge = df_merge['merged'].to_list()
df_bool = df_aggs_2['RegionCode'].isin(list_merge)
df_fit = df_aggs_2.loc[df_bool]
df_fit.columns = ['RegionCode','Lines']
df_fit = df_fit.reset_index(drop=True)

## kick duplicates (repeat)
list_org = []
for i in range(len(df_fit)):
    
    res = list(OrderedDict((tuple(x),x)for x in df_fit.Lines[i]))
    list_org.append(res)

# set up for geodataframe        
df_neighbours_test_2           = pd.DataFrame({0:list_org}) 
df_fit = df_fit.drop(['Lines'],axis=1)
df_aggs_2 = pd.concat([df_fit['RegionCode'],df_neighbours_test_2],axis=1)

# index and fit
list_merge = df_merge['merged'].to_list()
df_bool = df_aggs_2['RegionCode'].isin(list_merge)
df_fit = df_aggs_2.loc[df_bool]
df_fit.columns = ['RegionCode','Lines']
df_fit = df_fit.reset_index(drop=True)

# make points to lines
region_points = []
all_points = []
counts = []
lines = []
new_regions = []

#loop to arange data
for i in range(len(df_fit.Lines)):
    region_points = []
    num = len(df_fit.Lines.iloc[i])
    new = df_fit.RegionCode.iloc[i]
    
    # threshold for points
    if num <= 7:
        continue
        
    for j in range(num):        
        test =  Point(df_fit.Lines.iloc[i][j])
        num_thresh = len(df_fit.Lines.iloc[i][j])
        
        
        region_points.append(test)
    
    # make list of points           
    all_points.append(region_points)
    new_regions.append(new)

# little loop for converting points to lines in shapely format    
for i in all_points:
    lines.append(LineString(i))    

# nearest neighbour for interpolation of linestrings (get start and end point correct)
list_itrp = []

for k in range(len(lines)):

    x,y = zip(*list(lines[k].coords))
    
    x = np.asarray(x)
    y = np.asarray(y)
    
    points = np.asarray(np.c_[x, y])
    
    clf = NearestNeighbors(7).fit(points)
    G = clf.kneighbors_graph()
    
    T = nx.from_scipy_sparse_matrix(G)
    
    paths = [list(nx.dfs_preorder_nodes(T, i)) for i in range(len(points))]
    
    mindist = np.inf
    minidx = 0
    
    for i in range(len(points)):
        p = paths[i]           # order of nodes
        ordered = points[p]    # ordered nodes
        # find cost of that order by the sum of euclidean distances between points (i) and (i+1)
        cost = (((ordered[:-1] - ordered[1:])**2).sum(1)).sum()
        if cost < mindist:
            mindist = cost
            minidx = i
    
    opt_order = paths[minidx]
    
    
    xx = x[opt_order]
    yy = y[opt_order]
       
    point_xy = [Point(xy) for xy in zip(xx,yy)]
    linestring_xy = LineString(point_xy)
    list_itrp.append(linestring_xy)




# set up for geodataframe        
df_neighbours_org           = pd.DataFrame(list_itrp) 
df_neighbours_org.columns   = ['geometry']
df_neighbours_org      = GeoDataFrame(df_neighbours_org,geometry='geometry')    

     
# arange regions and pagree
new_pagree = pd.concat([df_merge,df_all.pAgree],axis=1)

df_bool_new = new_pagree['merged'].isin(new_regions)
df_fit_new = new_pagree.loc[df_bool_new]
        
# make four cases for line width

output_linewidth = [0.5 if v >0.8 else 1 if 0.8 > v > 0.701 else 1.5 if 0.7>0.601
                    else 2.5 if 0.60>v>0.21 else 0 for v in df_fit_new['pAgree']]

df_linewidth = pd.DataFrame(output_linewidth)
df_linewidth.columns = ['linewidth']


# plot layout
fig, ax = plt.subplots(figsize=(15,15))
df_neighbours_org.plot(ax=ax,linewidth=output_linewidth,color='black')
df.plot(ax=ax,linewidth=0.2)

fig.figsave('output/' + 'pagree_map')





