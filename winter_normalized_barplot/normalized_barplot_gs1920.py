#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 17:07:48 2020

@author: Schmulius
"""


import os
import matplotlib
import numpy as np
import pandas as pd
import sys
import csv
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.dates import DateFormatter
from matplotlib import rc
#import seaborn as sns
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.ticker as ticker
import matplotlib.dates as dates


# nicer settings for plot-layout
matplotlib.style.use('ggplot')
plt.rcParams['axes.facecolor']='white'




# import data as dataframe
path = 'input/avalanche_report_statistics.csv'

#number of regions
num_regs = 29

# make dataframe
df_saison1920 = pd.read_csv(path,header=0,sep=';')

# filter and arange dataframe
df_saison1920_tirol = df_saison1920.groupby(['Region']).get_group('AT-07')
df_saison1920_tirol_filter = df_saison1920_tirol[['Date','Daytime','Subregion',
                                                  'DangerRatingBelow',
                                                 'DangerRatingAbove']]
# replace strings with numbers
df_saison1920_tirol_filter = df_saison1920_tirol_filter.replace(to_replace =['low'],  
                            value ="1")
df_saison1920_tirol_filter = df_saison1920_tirol_filter.replace(to_replace =['moderate'],  
                            value ="2")
df_saison1920_tirol_filter = df_saison1920_tirol_filter.replace(to_replace =['considerable'],  
                            value ="3")
df_saison1920_tirol_filter = df_saison1920_tirol_filter.replace(to_replace =['high'],  
                            value ="4")
df_saison1920_tirol_filter = df_saison1920_tirol_filter.replace(to_replace =['very high'],  
                            value ="5")
df_saison1920_tirol_filter = df_saison1920_tirol_filter.replace(to_replace =['no_snow'],  
                            value ="0")

#drops the duplicates (both) and only saves the days without change over daytime
filter_am_oneday = df_saison1920_tirol_filter.drop_duplicates(subset=['Date','Subregion'],keep=False)
filter_pm_oneday = filter_am_oneday.replace(to_replace =['AM'],  
                            value ="PM") 


# filter am and pm
filtered_am = df_saison1920_tirol_filter.groupby(['Date','Subregion']).first()
filtered_pm = df_saison1920_tirol_filter.groupby(['Date','Subregion']).nth(1)

###############################################################################
# AM
###############################################################################

#concat for pm
am_all = filtered_am

# get counts below
am_all_counts_below = am_all.groupby(['Date','DangerRatingBelow'])['DangerRatingBelow'].count()
am_all_counts_below = am_all_counts_below.rename(columns={'DangerRatingBelow':'counts'},inplace = True)
am_all_counts_below = am_all_counts_below.reset_index()
am_all_counts_below.columns = ['Date','DR','counts']

# get counts above
am_all_counts_above = am_all.groupby(['Date','DangerRatingAbove'])['DangerRatingAbove'].count()
am_all_counts_above = am_all_counts_above.rename(columns={'DangerRatingAbove':'counts'},inplace = True)
am_all_counts_above = am_all_counts_above.reset_index()
am_all_counts_above.columns = ['Date','DR','counts']

# merge above and below
all_counts = pd.concat([am_all_counts_below,am_all_counts_above],ignore_index=True,axis=0)
all_counts_sum = all_counts.groupby(['Date','DR']).sum()
all_counts_sum = all_counts_sum.reset_index('DR')

# add normalized column, change df
new_df = all_counts_sum.pivot(columns='DR')
new_df.columns = ['schneefrei','1','2','3','4']

#get length's from row and normalize
df_len = new_df.sum(axis=1)
func = lambda x: np.asarray(x) / np.asarray(df_len)
new_df_normalized = new_df.apply(func)*100


#new datetime index
index = [pd.to_datetime(date, format='%Y-%m-%d').date() for date in new_df_normalized.index]
index = pd.Index(index)
#reset index
new_df_normalized.reset_index().drop(['Date'],axis=1)

new_df_normalized.index = pd.to_datetime(new_df_normalized.index).strftime('%Y-%m')

#new date range
idx = pd.date_range('2019-11-16', '2020-05-03')
new_df_normalized = new_df_normalized.set_index(idx)





# plots

fig,ax = plt.subplots(figsize=(13,7))
# y-axis in bold
rc('font', weight='bold')

barWidth = 0.85

#make colors for bar plot
colors = ["white",'#cdfd70','#fffd38','#fd9927','#fc0d1b']


new_df_normalized.plot(kind='bar',color=colors, stacked=True,ax=ax, edgecolor='black', width=barWidth,
                       linewidth=0.5)

ax.set_title('Gefahrenstufen Vormittag (AM)', size = 22, fontweight = 'bold')
ax.set_xlabel('Datum', size = 18, fontweight = 'bold')
ax.set_ylabel('Prozentualer Anteil', size = 18, fontweight = 'bold')



ticks = [tick.get_text() for tick in ax.get_xticklabels()]
ticks = pd.to_datetime(ticks).strftime('%b %Y')
ax.set_xticklabels(ticks)

# arange dates with
for label in ax.get_xticklabels():
        label.set_rotation(40)
        label.set_horizontalalignment('right')

# filter dates for x axis        
ax.set_xticklabels([dt.strftime('%Y-%m-%d') for dt in new_df_normalized.index])
for i, tick in enumerate(ax.xaxis.get_major_ticks()):
    if (i % (24) != 0): # 24 hours * 7 days = 1 week
        tick.set_visible(False)
 
ax.legend(loc=9,bbox_to_anchor=(0.5,-0.25),ncol=5,frameon=False)

plt.show()

fig.savefig('Output/GS_AM.png', bbox_inches='tight',dpi=300)
fig.savefig('Output/GS_AM.pdf', bbox_inches='tight',dpi=300)




###############################################################################
# PM
###############################################################################
#concat for pm
pm_all = pd.concat([filter_pm_oneday,filtered_pm.reset_index()],ignore_index=True,axis=0)

# get counts below
pm_all_counts_below = pm_all.groupby(['Date','DangerRatingBelow'])['DangerRatingBelow'].count()
pm_all_counts_below = pm_all_counts_below.rename(columns={'DangerRatingBelow':'counts'},inplace = True)
pm_all_counts_below = pm_all_counts_below.reset_index()
pm_all_counts_below.columns = ['Date','DR','counts']

# get counts above
pm_all_counts_above = pm_all.groupby(['Date','DangerRatingAbove'])['DangerRatingAbove'].count()
pm_all_counts_above = pm_all_counts_above.rename(columns={'DangerRatingAbove':'counts'},inplace = True)
pm_all_counts_above = pm_all_counts_above.reset_index()
pm_all_counts_above.columns = ['Date','DR','counts']

# merge above and below
all_counts = pd.concat([pm_all_counts_below,pm_all_counts_above],ignore_index=True,axis=0)
all_counts_sum = all_counts.groupby(['Date','DR']).sum()
all_counts_sum = all_counts_sum.reset_index('DR')

# add normalized column
new_df = all_counts_sum.pivot(columns='DR')
new_df.columns = ['schneefrei','1','2','3','4']

#get length's from row and normalize
df_len = new_df.sum(axis=1)
func = lambda x: np.asarray(x) / np.asarray(df_len)
new_df_normalized = new_df.apply(func)*100

#new datetime index
index = [pd.to_datetime(date, format='%Y-%m-%d').date() for date in new_df_normalized.index]
index = pd.Index(index)
#reset index
new_df_normalized.reset_index().drop(['Date'],axis=1)

new_df_normalized.index = pd.to_datetime(new_df_normalized.index).strftime('%Y-%m')


#new date range
idx = pd.date_range('2019-11-16', '2020-05-03')
new_df_normalized = new_df_normalized.set_index(idx)


###############################################################################
# Make Plot for PM
###############################################################################
fig,ax = plt.subplots(figsize=(13,7))
# y-axis in bold
rc('font', weight='bold')

barWidth = 0.85

#make colors for bar plot
colors = ["white",'#cdfd70','#fffd38','#fd9927','#fc0d1b']


new_df_normalized.plot(kind='bar',color=colors, stacked=True,ax=ax, edgecolor='black', width=barWidth,
                       linewidth=0.5)

ax.set_title('Gefahrenstufen Nachmittag (PM)', size = 22, fontweight = 'bold')
ax.set_xlabel('Datum', size = 18, fontweight = 'bold')
ax.set_ylabel('Prozentualer Anteil', size = 18, fontweight = 'bold')



ticks = [tick.get_text() for tick in ax.get_xticklabels()]
ticks = pd.to_datetime(ticks).strftime('%b %Y')
ax.set_xticklabels(ticks)

# arange dates with
for label in ax.get_xticklabels():
        label.set_rotation(40)
        label.set_horizontalalignment('right')

# filter dates for x axis        
ax.set_xticklabels([dt.strftime('%Y-%m-%d') for dt in new_df_normalized.index])
for i, tick in enumerate(ax.xaxis.get_major_ticks()):
    if (i % (24) != 0): # 24 hours * 7 days = 1 week
        tick.set_visible(False)
 
ax.legend(loc=9,bbox_to_anchor=(0.5,-0.25),ncol=5,frameon=False)

plt.show()

fig.savefig('Output/GS_PM.png', bbox_inches='tight',dpi=300)
fig.savefig('Output/GS_PM.pdf', bbox_inches='tight',dpi=300)




