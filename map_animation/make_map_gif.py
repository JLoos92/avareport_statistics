# -*- coding: utf-8 -*-
"""
Created on Thu May 28 10:00:50 2020

@author: Admin
"""
import os
import imageio
from PIL import Image,ImageDraw,ImageFont
import io
import numpy as np
import shutil
import glob
import fnmatch


# make dir for map .jpg
maps_dir = "maps/"


# write new pngs
for file_name in os.listdir(maps_dir):
    
    # change sfx
    im = Image.open(os.path.join(maps_dir,file_name))
    file = os.path.splitext(os.path.basename(file_name))[0]
    im.save(os.path.join(maps_dir,file) + '.png')


# rename all images
for root, dirs, files in os.walk(maps_dir):
    for filename in files:
        if 'fd' in filename:
            
            prefix = os.path.basename(root) 
            file_2 = os.path.splitext(os.path.basename(filename))[0]
            
            # new name for copy-file
            # new_name = os.rename(os.path.join(root, filename), os.path.join(root, "{}{}".format(file_2,'2.jpg')))
                       
            shutil.copy(maps_dir + filename,maps_dir + file_2 + '_2.jpg')
        
    
    
    
for file_name,i in zip(os.listdir(maps_dir),np.arange(1,340,1)):
    
    #get all png's
    if file_name.endswith('.png'):
            
        png = Image.open(os.path.join(maps_dir,file_name), mode= 'r')
        roiimg = png.crop()
        
        date = file_name[0 : 10]
        
        imgByteArr = io.BytesIO()
        roiimg.save(imgByteArr, format='PNG')
        imgByteArr = imgByteArr.getvalue()
        
        
        # create a PIL image object
        image = Image.open(io.BytesIO(imgByteArr))
        draw = ImageDraw.ImageDraw(image)
        
        # load a font
        font = ImageFont.truetype("arial.ttf", 56)
        font_2 = ImageFont.truetype("arial.ttf", 50)
        
        
        # draw title
        draw.text((400*4,15*4), 
                  date,
                  fill=(26, 71, 255), 
                  font=font)
        
        # draw Nov.
        draw.text((2*4,25*4), 
                  '16.11.',
                  fill=(26, 71, 255), 
                  font=(ImageFont.truetype("arial.ttf",30)))
        
        
        # draw Dez.
        draw.text((30*4,15*4), 
                  'Dec.',
                  fill=(26, 71, 255), 
                  font=font_2)
        
        # draw Jan.
        draw.text((91*4,15*4), 
                  'Jan.',
                  fill=(26, 71, 255), 
                  font=font_2)
        
        # draw Feb.
        draw.text((152*4,15*4), 
                  'Feb.',
                  fill=(26, 71, 255), 
                  font=font_2) 
        
         # draw Mar.
        draw.text((213*4,15*4), 
                  'Mar.',
                  fill=(26, 71, 255), 
                  font=font_2)
        
         # draw Apr.
        draw.text((274*4,15*4), 
                  'Apr.',
                  fill=(26, 71, 255), 
                  font=font_2)
        
        # draw May.
        draw.text((335*4,15*4), 
                  'May',
                  fill=(26, 71, 255), 
                  font=font_2)
        
        
        
        draw.line([(8*4,40*4),(348*4,40*4)],
                   fill = (26, 71, 255),
                   width = 6)
        
        
        # rectangle start
        draw.rectangle([(4*4, 36*4), (12*4, 44*4)], fill='white', outline= (26, 71, 255))        
        #Dec
        draw.rectangle([(35*4, 36*4), (44*4, 44*4)], fill='white', outline= (26, 71, 255))
        #Jan
        draw.rectangle([(97*4, 36*4), (106*4, 44*4)], fill='white', outline= (26, 71, 255))
        #Feb
        draw.rectangle([(159*4, 36*4), (168*4, 44*4)], fill='white', outline= (26, 71, 255))
        #Mar
        draw.rectangle([(217*4, 36*4), (226*4, 44*4)], fill='white', outline= (26, 71, 255))
        #Apr
        draw.rectangle([(279*4, 36*4), (288*4, 44*4)], fill='white', outline= (26, 71, 255))
        #May
        draw.rectangle([(341*4, 36*4), (350*4, 44*4)], fill='white', outline= (26, 71, 255))
        
        
        
        # draw marker for timeline
        draw.ellipse((4*4+(i*4),36*4,12*4+(i*4),44*4),fill = 'black',outline='black')
        
        
        # write to a png file
        filename = "img_maps/" + file_name
        image.save(filename, "PNG")
        
        
        
        
#providing the path of the folder
folder_path = (maps_dir)
#using listdir() method to list the files of the folder
test = os.listdir(folder_path)
#taking a loop to remove all the images
#using ".png" extension to remove only png images
for images in test:
    if images.endswith(".jpg"):
        os.remove(os.path.join(folder_path, images))
        
        
# make gif block
png_dir = 'img_maps/'
images = []
for file_name in os.listdir(png_dir):
    if file_name.endswith('.png'):
        file_path = os.path.join(png_dir, file_name)
        images.append(imageio.imread(file_path))
imageio.mimsave('movies/map_10s_24.gif', images,fps=24)