# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import os
import glob
from shutil import copy
from PIL import Image
import glob
import shutil

# rename all images
for root, dirs, files in os.walk("images_19-20"):
    if not files:
        continue
    prefix = os.path.basename(root)
    for f in files:
        os.rename(os.path.join(root, f), os.path.join(root, "{}_{}".format(prefix, f)))
        shutil.copy
        
    
#put them into a one folder    
dir_src = r"images_19-20"
dir_dst = r"images_19-20_all"
for file in glob.iglob('%s/**/*.jpg' % dir_src, recursive=False):
    copy(file, dir_dst)
    

#maps
dir_src = r"images_19-20_all"
dir_dst = r"maps"
for file in glob.iglob('%s/*map.jpg' % dir_src, recursive=True):
    shutil.copy(file, dir_dst)
    
#thumbnails
dir_src = r"images_19-20_all"
dir_dst = r"thumbnails"
for file in glob.iglob('%s/*thumbnail.jpg' % dir_src, recursive=True):
    copy(file, dir_dst)
    
    

# filter after res thumbs
dir_src = r"thumbnails"
dir_dst = r"double_thumbnails"
for file in glob.iglob('%s/*thumbnail.jpg' % dir_src, recursive=True):
    im = Image.open(file)
    print(im.size[0])
    if im.size[0] == 708:
        
        copy(file, dir_dst)
        im.close()
        os.remove(file)


# filter after res maps
dir_src = r"maps"
dir_dst = r"double_maps"
for file in glob.iglob('%s/*map.jpg' % dir_src, recursive=True):
    im = Image.open(file)
    print(im.size[0])
    if im.size[0] == 3780:
        
        shutil.copy(file, dir_dst)
        im.close()
        os.remove(file)