# -*- coding: utf-8 -*-
"""
Created on Wed May 27 13:33:53 2020

@author: Admin
"""

import os
import imageio
from PIL import Image,ImageDraw,ImageFont
import io
import numpy as np

# make gif block
png_dir = 'img_thumbnails/'
images = []
for file_name in os.listdir(png_dir):
    if file_name.endswith('.png'):
        file_path = os.path.join(png_dir, file_name)
        images.append(imageio.imread(file_path))
imageio.mimsave('movies/movie.gif', images,fps=10)



thumbnails_dir = "thumbnails/"
maps_dir = "maps/"


# write time in GIF
for file_name in os.listdir(thumbnails_dir):
    
    # change sfx
    im = Image.open(os.path.join(thumbnails_dir,file_name))
    im.save(os.path.join(thumbnails_dir,file_name) + '.png')
    
for file_name,i in zip(os.listdir(thumbnails_dir),np.arange(1,340,1)):
    
    if file_name.endswith('.png'):
            
        png = Image.open(os.path.join(thumbnails_dir,file_name), mode= 'r')
        roiimg = png.crop()
        
        date = file_name[0 : 10]
        
        imgByteArr = io.BytesIO()
        roiimg.save(imgByteArr, format='PNG')
        imgByteArr = imgByteArr.getvalue()
        
        
        # create a PIL image object
        image = Image.open(io.BytesIO(imgByteArr))
        draw = ImageDraw.ImageDraw(image)
        
        # load a font
        font = ImageFont.truetype("arial.ttf", 24)
        font_2 = ImageFont.truetype("arial.ttf", 16)
        
        
        # draw title
        draw.text((image.width - 140,305), 
                  date,
                  fill=(26, 71, 255), 
                  font=font)
        
        # draw Dez.
        draw.text((30,15), 
                  'Dec.',
                  fill=(26, 71, 255), 
                  font=font_2)
        
        # draw Jan.
        draw.text((91,15), 
                  'Jan.',
                  fill=(26, 71, 255), 
                  font=font_2)
        
        # draw Feb.
        draw.text((152,15), 
                  'Feb.',
                  fill=(26, 71, 255), 
                  font=font_2) 
        
         # draw Mar.
        draw.text((213,15), 
                  'Mar.',
                  fill=(26, 71, 255), 
                  font=font_2)
        
         # draw Apr.
        draw.text((274,15), 
                  'Apr.',
                  fill=(26, 71, 255), 
                  font=font_2)
        
        # draw May.
        draw.text((323,15), 
                  'May',
                  fill=(26, 71, 255), 
                  font=font_2)
        
        
        
        draw.line([(8,40),(348,40)],
                   fill = (26, 71, 255),
                   width = 3)
        
        
        # rectangle start
        draw.rectangle([(4, 36), (12, 44)], fill='white', outline= (26, 71, 255))        
        #Dec
        draw.rectangle([(35, 36), (44, 44)], fill='white', outline= (26, 71, 255))
        #Jan
        draw.rectangle([(97, 36), (106, 44)], fill='white', outline= (26, 71, 255))
        #Feb
        draw.rectangle([(159, 36), (168, 44)], fill='white', outline= (26, 71, 255))
        #Mar
        draw.rectangle([(217, 36), (226, 44)], fill='white', outline= (26, 71, 255))
        #Apr
        draw.rectangle([(279, 36), (288, 44)], fill='white', outline= (26, 71, 255))
        #May
        draw.rectangle([(339, 36), (348, 44)], fill='white', outline= (26, 71, 255))
        
        
        
        # draw marker for timeline
        draw.ellipse((4+i,36,12+i,44),fill = 'black',outline='black')
        
        
        # write to a png file
        filename = "img_thumbnails/" + file_name
        image.save(filename, "PNG")
            
            
        
        
        
#providing the path of the folder
folder_path = (thumbnails_dir)
#using listdir() method to list the files of the folder
test = os.listdir(folder_path)
#taking a loop to remove all the images
#using ".png" extension to remove only png images
for images in test:
    if images.endswith(".jpg"):
        os.remove(os.path.join(folder_path, images))