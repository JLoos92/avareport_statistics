#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 08:56:17 2020

@author: Schmulius
"""

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats
from scipy.stats import norm
#sns.set(color_codes=True,rc={'figure.facecolor':'white','axes.facecolor':'white'})


# import data as dataframe
path = 'input/avalanche_report_statistics.csv'

# make dataframe
# make dataframe
df_saison1920 = pd.read_csv(path,header=0,sep=';')
df_saison1920 = df_saison1920.drop_duplicates(subset=['BulletinId'])
# filter and arange dataframe
df_saison1920_tirol_filter = df_saison1920[['Date','Daytime','Subregion',
                                                  'DangerRatingBelow',
                                                 'DangerRatingAbove']]
# replace strings with numbers
df_saison1920_tirol_filter = df_saison1920_tirol_filter.replace(to_replace =['low'],  
                            value ="1")
df_saison1920_tirol_filter = df_saison1920_tirol_filter.replace(to_replace =['moderate'],  
                            value ="2")
df_saison1920_tirol_filter = df_saison1920_tirol_filter.replace(to_replace =['considerable'],  
                            value ="3")
df_saison1920_tirol_filter = df_saison1920_tirol_filter.replace(to_replace =['high'],  
                            value ="4")
df_saison1920_tirol_filter = df_saison1920_tirol_filter.replace(to_replace =['very high'],  
                            value ="5")
df_saison1920_tirol_filter = df_saison1920_tirol_filter.replace(to_replace =['no_snow'],  
                            value ="0")

df_saison1920_tirol_filter = df_saison1920_tirol_filter.drop(['Date','Daytime','Subregion'],axis=1) 

df_max = df_saison1920_tirol_filter.max(axis=1)
df_max = df_max.to_frame()
df_max.columns = ['Stufen']
df_max = df_max['Stufen'].value_counts().reset_index()
df_max.loc[-1] = [5,0]

color_palette = ['#cdfd70','#fffd38','#fd9927','#fc0d1b','black']

                 

fig, ax = plt.subplots(figsize=(15,15))  
sns.set_style('whitegrid')               
ax = sns.barplot(x='index',y='Stufen',data=df_max,palette=color_palette)
ax.set_xticklabels(['1','2','3','4','5'])

# Change for country
plt.xlabel('Scala di pericolo',fontsize=15)
plt.ylabel('Frequenza',fontsize=15)

plt.show()
plt.savefig('Output/histogram_1920_italy.pdf')
plt.savefig('Output/histogram_1920_italy.png')

