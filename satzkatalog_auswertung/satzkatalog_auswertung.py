"""
Created on Mon Nov 11 14:53:07 2019

@author: Schmulius
"""

import pandas as pd
import numpy as np
import csv
import matplotlib
from pylab import *
import numpy as np
from pandas import DataFrame, Series
from scipy.spatial.distance import pdist, squareform
import matplotlib.pyplot as plt
from numpy import genfromtxt
import sys
import re
import pandas as pd
from datetime import date
import datetime
import locale
import time
from functools import partial
import matplotlib.dates as mdates
import os
import matplotlib.ticker as ticker
from matplotlib.ticker import NullFormatter
from matplotlib.dates import MonthLocator, DateFormatter
from matplotlib import rc,rcParams





# Auswertung Satzkatalog; Daten einladen
data_sentence = pd.read_excel('input/Satzkatalog_Rohdaten.xlsx',sheet_name='Sentence IDs')
data_bulletins = pd.read_csv('input/Satzkatalog_Rohdaten_1920.csv',sep=';')
data_bulletins = data_bulletins.drop_duplicates(subset='BulletinId')

#Read activity highlights
activity_highlights = data_bulletins['AvActivityHighlightIdsDe'].dropna()
activity_comments = data_bulletins['AvActivityCommentIdsDe'].dropna()
snowpack_structure = data_bulletins['SnowpackStructureCommentIdsDe'].dropna()
tendency = data_bulletins['TendencyCommentIdsDe'].dropna()
authors = data_bulletins['Author']

#Split first
sentence_id = activity_highlights.str.split('[',expand=True)[0].to_frame()
activity_comments_id = activity_comments.str.split('[',expand=True)[0].to_frame()
snowpack_structure_id = snowpack_structure.str.split('[',expand=True)[0].to_frame()
tendency_id = tendency.str.split('[',expand=True)[0].to_frame()


##Split second and so on
sentence_id_2 = activity_highlights.str.findall(r"\d+").dropna()
activity_comments_id_2 = activity_comments.str.findall(r"\d+").dropna()
snowpack_structure_id_2 = snowpack_structure.str.findall(r"\d+").dropna()
tendency_id_2 = tendency.str.findall(r"\d+").dropna().reset_index()


##Split second and so on
sentence_id_2 = activity_highlights.str.findall("\.(.*?)\[").dropna().reset_index()
activity_comments_id_2 = activity_comments.str.findall("\.(.*?)\[").dropna().reset_index()
snowpack_structure_id_2 = snowpack_structure.str.findall("\.(.*?)\[").dropna().reset_index()
tendency_id_2 = tendency.str.findall("\.(.*?)\[").dropna().reset_index()

##############################################################################
def help_func(id_name, name):
    
    list_2 = []
    for i in range(id_name[name].size):
        new = list(map(int,id_name[name][i]))
        list_2.append(new)
    
    # Flat list
    flat_list = []
    for sublist in list_2:
        for item in sublist:
            flat_list.append(item)
    
    #Flatlistarray
    flatlist_array =  np.asarray(flat_list)
    flatlist_array_1 = flatlist_array[(flatlist_array<517)]
    
    df_tend = pd.DataFrame(flatlist_array_1)
    df_tend.columns = ['ID']
    
    # counts
    df_tend['freq'] = df_tend.groupby('ID')['ID'].transform('count')
    df_tend_counts = df_tend.drop_duplicates().sort_values(by = 'freq')
    
    return df_tend_counts


##############################################################################

# To pandas frame
sentence_id.columns = ['ID']
activity_comments_id.columns = ['ID']
snowpack_structure_id.columns = ['ID']
tendency_id.columns = ['ID']


# counts
sentence_id['freq'] = sentence_id.groupby('ID')['ID'].transform('count')
sentence_id_counts = sentence_id.drop_duplicates().sort_values(by = 'freq')

activity_comments_id['freq'] = activity_comments_id.groupby('ID')['ID'].transform('count')
activity_comments_id_counts = activity_comments_id.drop_duplicates().sort_values(by = 'freq')

snowpack_structure_id['freq'] = snowpack_structure_id.groupby('ID')['ID'].transform('count')
snowpack_structure_id_counts = snowpack_structure_id.drop_duplicates().sort_values(by = 'freq')

tendency_id['freq'] = tendency_id.groupby('ID')['ID'].transform('count')
tendency_id_counts = tendency_id.drop_duplicates().sort_values(by = 'freq')


all_counts = sentence_id_counts.append(activity_comments_id_counts,ignore_index=True)
all_counts = all_counts.append(snowpack_structure_id_counts,ignore_index=True)

all_counts = all_counts.append(help_func(id_name=sentence_id_2,name = 'AvActivityHighlightIdsDe'),ignore_index=True)
all_counts = all_counts.append(help_func(id_name=activity_comments_id_2,name = 'AvActivityCommentIdsDe'),ignore_index=True)
all_counts = all_counts.append(help_func(id_name=snowpack_structure_id_2,name = 'SnowpackStructureCommentIdsDe'),ignore_index=True)
all_counts = all_counts.append(help_func(id_name=tendency_id_2,name = 'TendencyCommentIdsDe'),ignore_index=True)
 
all_counts = all_counts.append(tendency_id_counts)


# Sum all counts
all_counts = all_counts.groupby('ID').sum()
all_counts = all_counts.sort_values(by='freq')

# Sentence with 15th most occurence
#all_counts_tail = all_counts.tail(15)
all_counts_tail = all_counts
#all_counts_tail = all_counts_tail.sort_index(axis=0)
all_counts_tail = all_counts_tail.reset_index()
all_counts_tail['ID'] = all_counts_tail.ID.astype(int)

all_counts_tail = all_counts_tail.sort_values(by='ID')
all_counts_tail = all_counts_tail.set_index('ID')

# Index sentences
index_nums = all_counts_tail.index

# new
sentence_name_id = data_sentence[['sentence_id', 'name','header','language']]

# select sentences real
real_sentences_tail = data_sentence[data_sentence['sentence_id'].isin(all_counts_tail.index)]
real_sentences_tail = real_sentences_tail.set_index('sentence_id')
real_sentences_tail = real_sentences_tail['name']

# select italy
real_sentences_tail_it = data_sentence[data_sentence['sentence_id'].isin(all_counts_tail.index)]
real_sentences_tail_it = real_sentences_tail_it.set_index('sentence_id')
real_sentences_tail_it = real_sentences_tail_it[['header','language']]

# concat de
all_names = pd.concat([all_counts_tail,real_sentences_tail],axis=1)
all_names = all_names.sort_values(by='freq')
all_names = all_names.groupby('name').sum().reset_index()
all_names = all_names.sort_values(by='freq',ascending = False)

# concat it
all_names_it = pd.concat([all_counts_tail,real_sentences_tail_it],axis=1)
all_names_it = all_names_it.sort_values(by='freq')
all_names_it = all_names_it.groupby('header').sum().reset_index()
all_names_it = all_names_it.sort_values(by='freq',ascending = False)

names_it = sentence_name_id[sentence_name_id['language'].str.contains("it")]
bool = names_it.loc[names_it['name'].isin(all_names['name'])]

bool_new = bool.reset_index(drop=True)
allnames_new = all_names.reset_index(drop=True)
allnames_new.columns = ['namede','freq']
all = pd.concat([allnames_new,bool_new], axis = 1)

new = all.filter(['namede','name','header'],axis=1)

#rearrange
name_it = new[['name','header']]
name_it.sort_values('name',inplace=True)
name_de = all_names.sort_values('name',inplace=True)
name_it = name_it.reset_index(drop=True)
name_de = all_names.reset_index(drop=True)

all_langues = pd.concat([name_de,name_it],axis=1)
all_langues = all_langues.sort_values(by='freq', ascending = False)
all_langues.columns = ['name','freq','name_2','header']


# Plots
# activate latex text rendering

fig, ax = plt.subplots(figsize=(12,2))

ax.bar(all_langues['name'],all_langues['freq'], width = 0.7) # Plots
#ax.invert_yaxis()  # labels read top-to-bottom
ax.set_ylabel('Counts')
ax.set_xlabel('Sentence')
ax.tick_params(axis='both', which='major', labelsize=6)
#ax.set_title('Verwendete Sätze (Satzkatalog 18/19)')
ax.set_xlim(-1,all_names['name'].size)
plt.xticks(rotation='vertical')

ax2 = ax.twiny()
ax2.set_xlim(-1,all_names['name'].size)
ax2.set_xticklabels(all_langues['header'],rotation=90,size=2)
ax2.set_xticks(np.arange(all_langues['header'].size))
ax2.tick_params(axis='both', which='major', labelsize=6)
#ax.grid(True)
plt.show()

path = str('output/')
fname_pdf = (str('Satz_auswertung_1920') + '.pdf')
fname_png = (str('Satz_auswertung_1920') + '.png')    
fig.savefig(path + fname_pdf, format = 'pdf',dpi=1000,bbox_inches = 'tight') 
fig.savefig(path + fname_png, format = 'png',dpi=1000,bbox_inches = 'tight') 



###############################################################################

# ALL for authors
all_columns = pd.concat([activity_highlights,activity_comments,snowpack_structure,
                         tendency, authors], axis = 1)

    
# To pandas frame
sentence_id.columns = ['ID','freq']
activity_comments_id.columns = ['ID_1','freq_1']
snowpack_structure_id.columns = ['ID_2','freq_2']
tendency_id.columns = ['ID_3','freq_3']
   
   
all_columns_id =  pd.concat([sentence_id,activity_comments_id,snowpack_structure_id,
                         tendency_id, authors], axis = 1)
    
all_columns.groupby('Author')

all_counts = all_columns_id.groupby(['ID','ID_1','ID_2','ID_3','Author']).size().sort_values(ascending=False)

counts_authors = all_columns_id.groupby(['ID','Author']).size().sort_values(ascending=False)
counts_authors_1 = all_columns_id.groupby(['ID_1','Author']).size().sort_values(ascending=False)
counts_authors_2 = all_columns_id.groupby(['ID_2','Author']).size().sort_values(ascending=False)
counts_authors_3 = all_columns_id.groupby(['ID_3','Author']).size().sort_values(ascending=False)

all_authors = counts_authors.append([counts_authors_1,counts_authors_2,counts_authors_3])

all_authors = all_authors.reset_index().groupby(['ID','Author']).sum().reset_index()
all_authors = all_authors.groupby(['Author','ID']).sum()























##############################################################################
def help_func_2(id_name, name):
    
    list_2 = []
    for i in range(id_name[name].size):
        new = list(map(int,id_name[name][i]))
        list_2.append(new)
    
    # Flat list
    flat_list = []
    for sublist in list_2:
        for item in sublist:
            flat_list.append(item)
    
    #Flatlistarray
    flatlist_array =  np.asarray(flat_list)
    flatlist_array_1 = flatlist_array[(flatlist_array<517)]
    
    df_tend = pd.DataFrame(flatlist_array_1)
    df_tend.columns = ['ID']
    
    # counts
    df_tend['freq'] = df_tend.groupby('ID')['ID'].transform('count')
    df_tend_counts = df_tend.drop_duplicates().sort_values(by = 'freq')
    
    return list_2

####################### Plot  #################################################

#1#############################################################################
list_highlights = help_func_2(id_name=sentence_id_2,name = 'AvActivityHighlightIdsDe')
list_highlights = pd.DataFrame(list_highlights)
list_highlights = list_highlights.set_index(sentence_id.index)
highlights_authors = authors.loc[sentence_id.index]

list_highlights = pd.concat([list_highlights,highlights_authors],axis=1)
list_highlights.columns = ['A','B','C','D','Author']

df_1 = list_highlights[['A','Author']] 
df_2 = list_highlights[['B','Author']] 
df_3 = list_highlights[['C','Author']] 
df_4 = list_highlights[['D','Author']] 

df_2.columns = ['A','Author']
df_3.columns = ['A','Author']
df_4.columns = ['A','Author']

list_highlights_all = df_1.append([df_2,df_3,df_4])


##2#############################################################################
##1
#list_highlights = help_func_2(id_name=activity_comments_id_2,name = 'AvActivityCommentIdsDe')
#list_highlights = pd.DataFrame(list_highlights)
#list_highlights = list_highlights.set_index(activity_comments.index)
#highlights_authors = authors.loc[activity_comments.index]
#
#list_highlights = pd.concat([list_highlights,highlights_authors],axis=1)
#list_highlights.columns = ['A','B','C','D','Author']
#
#df_1 = list_highlights[['A','Author']] 
#df_2 = list_highlights[['B','Author']] 
#df_3 = list_highlights[['C','Author']] 
#df_4 = list_highlights[['D','Author']] 
#
#df_2.columns = ['A','Author']
#df_3.columns = ['A','Author']
#df_4.columns = ['A','Author']
#
#list_highlights_all = list_highlights.append([df_1,df_2,df_3,df_4])
#
#
